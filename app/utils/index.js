"use strict";

const XLSX = require('xlsx');
const fs = require('fs');
var path = require('path');
const pdfMakePrinter = require('pdfmake/src/printer');
var Printer = require('pdfmake');
const _ = require('underscore');
const moment = require('moment');
var async = require('asyncawait/async');
var await = require('asyncawait/await');

// fonts are available in the test-env/tests/fonts path, this is a helper
function fontPath(file) {
  return path.resolve('pdfmake', 'test-env', 'tests', 'fonts', file);
}

const uploadsPath = path.resolve('uploads');

/**
 * Utils.
 *
 * @module
 */

module.exports.readExcel = () => {
    return getExcel();
};

const getExcel = () => {
    let excelContent;

    fs.readdirSync('./uploads').forEach(file => {
        const workbook = XLSX.readFileSync(path.resolve(uploadsPath, file));
        const sheetName = workbook.SheetNames;
        excelContent = (XLSX.utils.sheet_to_json(workbook.Sheets[sheetName[0]]));

        fs.unlinkSync(path.resolve(uploadsPath, file));
    })

    return excelContent;
}

module.exports.generatePdf = (docDefinition) => {

  return new Promise(function(resolve, reject) {
    try {
      const fontDescriptors = {Roboto: {
          normal: fontPath('Roboto-Regular.ttf'),
          bold: fontPath('Roboto-Medium.ttf'),
          italics: fontPath('Roboto-Italic.ttf'),
          bolditalics: fontPath('Roboto-Italic.ttf'),
        }};
      const printer = new pdfMakePrinter(fontDescriptors);
      const doc = printer.createPdfKitDocument(docDefinition);
      
      let chunks = [];
  
      doc.on('data', (chunk) => {
        chunks.push(chunk);
      });
    
      doc.on('end', () => {
        const result = Buffer.concat(chunks);
        resolve('data:application/pdf;base64,' + result.toString('base64'));
      });
      
      doc.end();
      
    } catch(err) {
      reject();
    }
  });
};

module.exports.splitPdfs = function(fileContent, callback) {
  let pdfLinks = [];
  let clients = _.groupBy(fileContent, "Client");

  Object.keys(clients).forEach(client => {
    pdfLinks.push(clients[client]); 
  })

  return await (callback(pdfLinks));
}

function getJsDateFromExcel(excelDate) {

  // JavaScript dates can be constructed by passing milliseconds
  // since the Unix epoch (January 1, 1970) example: new Date(12312512312);

  // 1. Subtract number of days between Jan 1, 1900 and Jan 1, 1970, plus 1 (Google "excel leap year bug")             
  // 2. Convert to milliseconds.

	return new Date((excelDate - (25567 + 2.04166))*86400*1000);
}

module.exports.splitToDocs = (arrayOfClients) => {
  let pdfLinks = [];

  console.log(arrayOfClients.length);

  try {
    for (let index = 0; index < arrayOfClients.length; index++) {
      let docContent = [];
      let clientsContent = [];
      docContent.push(	[{text: 'Client', style: 'tableHeader'}, {text: 'Event', style: 'tableHeader'}, {text: 'Terminal', style: 'tableHeader'}, {text: 'Event date and time', style: 'tableHeader'}, {text: 'Product/Service', style: 'tableHeader'}, {text: 'Notes', style: 'tableHeader'}, {text: 'Card holder', style: 'tableHeader'}, {text: 'Quantity', style: 'tableHeader'}])
      let clients = arrayOfClients[index];
      let overalAmount = 0;
      let billDate = clients[0]['Event date and time'];
      clients.forEach(client => {
          if (client.Client) {
            overalAmount += client.Quantity;
            if (client['Card holder']) {
              clientsContent.push([client.Client, client.Event, client.Terminal, moment(getJsDateFromExcel(client['Event date and time'])).format('DD.MM.YYYY HH:mm:ss'), client['Product'], client.Notes, client['Card holder'], client.Quantity]);
            } else 
              clientsContent.push([client.Client, client.Event, client.Terminal, moment(getJsDateFromExcel(client['Event date and time'])).format('DD.MM.YYYY HH:mm:ss'), client['Product'], client.Notes, "", client.Quantity]);
          }
      });

      function Comparator(a, b) {
          if (a[3] < b[3]) return -1;
          if (a[3] > b[3]) return 1;
        return 0;
      }

      clientsContent = clientsContent.sort(Comparator);

      let arraysToConcat = [docContent, clientsContent];
      let mergedArray = Array.prototype.concat.apply([], arraysToConcat);

      mergedArray.push(	[{text: '', style: 'tableHeader'}, {text: clients.length, style: 'tableHeader'}, {text: clients.length, style: 'tableHeader'}, {text: '', style: 'tableHeader'}, {text: '', style: 'tableHeader'}, {text: '', style: 'tableHeader'}, {text: '', style: 'tableHeader'}, {text: overalAmount.toFixed(2), style: 'tableHeader'}])
  
      if (mergedArray.length) {
        let docDefinition = {
          header: {
            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASEAAABUCAYAAADNhYHdAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAhIElEQVR42ux9fVDc533n57FBNovOLGK5FipgLSMhNRUsxnYsMrKWSG57OdmC6QVf59pomUudseNUwspUtptGIp5r7ak9yBPXHrvTY5WbudTSZIQlTa+5IAlFMVIaEy3YqUBC1rJLwS0L2pXFYgkyT//4vT1vv91FrJLc5fnMrEDL7u/lefk8n+/L8/0RSik0NDQ0flm4QzeBhoaGJiENDQ1NQhoaGhqahDQ0NDQJaWhoaGgS0tDQ0CSkoaGhoUlIQ0NDk5CGhoaGJiENDQ1NQhoaGhq3AwXL+XIknqK95ycxNJFCJJZEdCYNECJ9zr+qCIFqL7as8yFYV45AVQnRTa+hoQEAZKkbWKMzaRoeGMfBgZhBOjYoQMGTkHhsYnzM7yvGzuZq7N5aC6+nUBOShoYmoexIphdo56FhhN8bZ75NHLIRf6fUJh2VOgIAr6cQu7bep8lIQ0OTUGYc6BujXcdHkJy7qSYguBCNRUQgiveJ/dPrKUBP6AG0Bio0EWloaBLi1U9HeBC9kSmRXRj7SlBChvThCUhJVJQ5jCGZQs016G6v16pIQ0OTkEFALa+eQSSWVCsfURFZ5ANgY/FduL98JQ5eSTgmmcVZNLNqClR5cWrPZk1EGhq/JrgjIwHFUwZhWKYTS0As+VDKmFwUX9+6Ft/4bw08NxFi/MKRmXg8gkgsiZZXzyCZXtAlHzU0fh2VkEFAP0QkluL8No7JxUbBWNPLUUHD3V8AALzw5k/wV+cnBDNOYZ4pTLdAVQlOff2RX6giis6kaTSRRv/oNECAVHoBkXgKXk8hGqpK7FsI1pXD6ynUqQb/DyCZXqCReFKU4vbPYJ1P9+GvGgm1vXGO2j6gXCNdzDFO/snDaHmoEgAwc/UG/N/4Pq4vLDIcpPIjQSY7aviIejqabusgicRT9ODZGHrPT/IpByzpwu0SKLrb67F7W60eyL8iiM6kae/5SZy+mED/xWkk5xYUY5bxD1BqLCjVXvjLPOgJ8eMtPDBOD56NSW5Qdk3tbq/XC9IywCUrHugbo73nJxUml9n6RNGJzOeeb1xtExAAlJXehf/1X+rR9t3zaiXFEpGC3MJnY9gRqKCtjZV57+D+0WnadXwE/aMJmRRZQuQGnnztyfSCHkW/AuD7k0pjU3YlOGo8mV4w1C8IekJNIqmhf2TaNc0ElMJf5tEdkA8SSqYXaNfxC1lzeyR1ZL63ceXdePaJjdLHW1v8eOb8FF4fmXIGhyulUIkQOsKDCNaV03yZZU7Eb1KtcJTEyJAPofxA1uvfL135dIQHTaKw1I1lcTH9I41r0bWQDaIbwlmddBBlebAd052Hhs1VXdSbQvhdVDAmer78EHyr7laeZH9HI4JlK8GF9LnOpbLAMt9Lzi/iwImxvJle977wfTPlgLgTD+eEt4iTOA54qtLmGr9o9EamaOOLJ0wfnqIvrGAIhdCHgqBXuBUUB2M+Q4T0Eo1lk5CxFSMmy1WqECpiR1GK1x9dj6bP+FxPUlZ6N175o/uxsvBOntTEvCGbBMANmNdOjC07WhaJp6gZdRMGHFUPRFvGQyZg5QjW+EUiPBCjbW+cQzK9yHSFwgSTTGuiICqamVDY9wW/ZcAKWGgsj4TCA+MK0iHMSiLY00ynPLPhN/HV9t/OeqKmz/jwnT+o51cUykpcIne6+TM5t2CaT7dugrW88kOTgCg/mKAYlKwa4uQ7pP8PxVN6FP0SFFBHz/vZP8iOVZtsVGkmRE4ZcVuYBNXsLSrUHZIPn9BBdj+YMhSv9pkEVxVjf8f9OZ+s7fN+fDuRxtf6Rpl5TFzOC44AXzsxhlBzzS3dZNub55CcX+SsK+XKZ16Dv8yD1sZK7GiogN/ngb/MQywyi8RTiM6kMTSRQv/ovy3LMR2dSdP+0Wk7KtcaqMw5ymKFnvsvJjgzNrC6BH5fcV6iNf2jCRqdSSM6M8e97y8rhr/Ms+Q0hehMmkbiRsUFNlruLSo0FUX2kLnl03NVKqJ5TSm8xYUIritHQ1WJfe3WfY0n0ojOpm2Hdv/oNA3WldsHHJ9Jy1Y3m1JC3NpummU7BKpKkE+/Jpt2wI7R24lIPEWT6ZtYSmqD8x2AbVeOhKIzaRqdnVdEoq0GVshZSrHyrgIc/rNHUFZ695Ju5Jn238boRAqvj3zsrD5S2JNI5ngknkIyvbBkB3X/aIJKETApEmb86i1egX3b17uG3L2eQhKs82U83/6jF2jX8RH1hABFaFMNup+oR9fxERzouySZnpnkvVXB4N3IlJFIynYa5R3mXs8K2tpQgZ3N1a6d73aOrmMXEB6I8Y5diNFCux2pv8yDI08/7EpI+49doAcHxhGdmRd8gVAez+spQHd7A0LN1dLxOg8NG3sYlcTA963XU4h929cj1FyTEwH0jyao2P7GAkF43yCDQJXXJoYDJ8aYvoF0r/4yD229/7ewa+t9SyINK+3g3ciU4/+St0rRQFUJgnU+7NxUk3VxaHnlDAUBtqz1cW05FE8hOb+AU3s2E8vsfa3vknNPzryhgWovdm2tlfopPDBO341Moff8pNhe1L+qCMH15dj32Aa7DQqMkLyQLEhEGcr/feVdBeh/qtnVEZ0N3352Ex48fgkj8RQufJzCg/eW4fr8Ai5MXcPlT27gg7kbTiPbaomif3QarY2VSzpX17ELCjKAZON7iwtxas/m5SsIAvX57M26hbC3wxDCqbKUpdYUK9+BE2PoOjYCPseFWSSEdIpkegHhs+MIn40hWOejPaGmrAM/PBCjnYeG+AAFuyiwFg1xlGt0Zk6pCLnM+yztD8YiT6YXcXBgHKHmagUJx9z3IHLkULLk7T+ZV3dRpRvvldxd4PgbbXIUtzIZ/RydTeNA3xjCA+Pobq+nKpIV26/r2AUcOHFZvV3KblNiqQ5E4ikc+MEYWhsraU+oyfX++y9OGz9HpuV+oMa5294856SwKPy3kXgKHeFBvHbiEj215xHb6pCP6ZB4dHYe4ffG0Xt+Et1P1NNQcw0pGJ+dl00iiQB45+zRLz2Y0RGdC760fS3avnkCvVMp9E5dw8rCO7GtrBh/2FCJb9WUIjV3A70/iaN3MmU3TGQitSQSsswdzs+j2P3vLV6RHwJyNRGcjjjQN+ZMOKG97X16wj20vXEWkfg1XpEQuJRPEX14QP/oNBq/dQI9HU20NaDOuYrOpGnnO0NIzi+A3/CXRaVC4WthVIubKnC+SrNMdAevnbicOYhgEdDqe/K7/5Cd7IRXn0MTKSNwMr/INBtlAixi+1Ek0wvoCA/C6ymkbpUjLD9mZCIFebsTFMm0/Ml6z08iEk/iyNObqOu4pgqnu/kHefGg6oUJQCSWgpGjNe1s9XJbnZmod+c7wwjWldM7uIEvDmQqRIEo8D82+dHyUEVe+vav//sD9gVfX/g5ej++hhcGrqDtuz/FqyfHsPUzFdh5r8++pqU6gQ05KBABlZXDvu3r80tAbIhfmfAp7KMTiYMZiI0vnnQGg+0sF0YSUd0ff9xkegFtb/wYkXhK6X197cRlYyJlijRx8p8yzlziYjrGFIEASApQmBWufhYnkZbKqpN5/8hXN+WRgJBhwzVB7/lJx98Iop5HoIqyNhSdfz+EzAR0TR2RVgRIpDWAEEQTaXSE33ePLItKnQnGcLzAcgFLgsw4P9A3ZnxHjB4T98BWcn4RXccumHlCIrtyE9U5yM57ffhK64a8zdXamhI8s/43mBXS6bAPPvkUz/9wDA/U+uwV01ilc8fpiwl5lRVIyespzP+2C9E8olD/Ljo6hauwJT6IoHwAeR8BK6fFMikOkbT9zVnloLQnuIoQudwayufdmMfu59qaJQwhBM6SHIU6AulCatHEHH9vVFZYux+tza+TlkAmSS5CBp6YCRQJr6r7IojOppWLQuehYYOAOLKgajJS7sN0zh2Jp9R5dipyI4qgjTWW2Q3o3HuKz1LBRJaqrTr3Ep1JoyA6M6c+MYONK+/Gf62vQEnxXUt2RGdD6PfW4fWRf+UHJ6GoX3k3/ueXH8LZD/8t8wTOZI4l5oTsWWsAOau36HfIqxpS5kEJ0RWofSXhgRg1pC3UWdxMSNnrKYTXs4LZ+0bV2d6U2n6J/Y9vkB2w7OKjOGeouRoNq0sQqCqxAgU4fSnh+A3Y1Xx+gWlzRrExJligqgQ7ApUI1pUjmb6JSDyF8dk0eiNTRt9xfZl28Smxfipg19ba2xgfIpxp7WRnC2qQCKQl+UfAKVS1ghTmJNOHoeZq27GbTC/QzneGET4bk/25Ztu/1jeG/Y9t4M6hXHCoi9WQdYxTuGegC8TNnpNSROIpFPAOTnkVer5xNT6ZX8Sfn43i5Jc/m/duNXxLzk2vXHEnnn+gGk8+vh4/GvoYz58eU6iYHEOKE9fUbE2dTPAt68pv03gl7hE5woaOfWioKsGQuVvfcaj/Mx+NEgcEMcK+3e31tkPV8OsMQ9qATHjldPBsTCIhb1EBk8Yg+wFCzdXc5s5gXTlnPihVqttANietWCXB8vf1SCFuw6+ljm46be1fVZT3ULW8b0xQo0yyY2D1PdjRWInxmTR6I5Pm5lnBDyb688RAytELGaKeRruz/eD1FJKejiZEJlJm2F7OezP2xiWoPU4SabmfKZRVM0KbqgECo6yzqj9FJUuMseL1rED4vahjvbj4ngJVJSiwcibExL2VhXfi9f+0AeEfXUH/rLEq1a8tuy3ztbWiBMkbi+jYvAb/ubkayU9uoCv8U7x+4WOF7F2WcS8RRN6TzSjUfhIhUhBqNkL1Kt9F/+g0tcPZLv4i/6oiyfnqL/OQI08/jMYXT1I7Fwey7I7OzCEST3EOy2BduRNSlZL4DPNq56Zqqgr3ez2FhCVQwMh56jo24l6FgVJ0HhqWNowy10PcTQU12fvLi2/fYgKViWEp0RU48tTDXHStO11P733hH42M7oz7JQWz2NrTqFQhFN1P1Cu/tyNQ4QQBFIooEk9CSi9hFxpCeXMewKk9m+17alhdQjsPfSD4BWUy7W7faLs3dm29j977/PdlMmUVMgUKuAiF+ceWsmL86e+uwx9/bxjXb/7cvriy0rvy2r+XY9fwf85NYGfwPrS2+HE5dg37e37K5BAJUaylEoa0YmYwk/Iu2xXkY74d2pS5REn/aEIRBeHJdN/jG1ydr/u2r0fbG+eE1Zrxu5mDks2J2bW1Fna6Bohk1iXnF9Hyyhnse3wD3f/YhqxTKlBVQgJVJcbqbC8klFMv4ffGEYmn6JGnH86uYCjk+5HU5m2ywljFIGXTU/SEmqTwvtdTSHZtraVdx0bk/CqXROD+0WlqBAeoi3IwSKo3MknFtfW0wiRmG4RXqlRemMEr5lBzDXdPoeYadB76QAiyUI6g/b5izr/qL/OQ4Dof7b+YYW8fAQoCVV7GpqeoX3kXvtH6O9jxnfdx/eaibMfliJmrN/Dl7h+h9+NrtqrauX0tTv7TJPoG/wVnx6+if3YOxYV34s8frMbBfSfQO3VNbTuaN9Bwq/t0FKaQJYn5BLrlI2X5Qtx8bNR9NbMwNJES+EwOw7YG3FMVAtVe9zCpef+2JLeVh4/sfnQtNRIoKR+UYPqi69gI3j0/SY98dVNW4ugJNTHOdfATGQ4ZNr54Et3tG2mouYZk5XWVo/gWxuetjR1ZmQSqvK4PaLASGZUFAInsXXdyclyUBjXa3y3VRDal3ZQ0UfpH2ejkDiEVxlzwqMrPaJFJa6DC3WdH4JpScUfD6hJuF/uzn1+L3e9EDAISbnLwZ4mc++67/3cMvVNGaPn6wiJCRz/Ef3jmKLb+7TkcvziN3/mPK3HkDxtx/W8ex98PTzoEJJoPjA8nsHppJBRcX66I8PCmxmkzaStfiEykeJNGyINpbazMGj5Ozi8wRAwpbJ5tC4C/zENcI1zU3bfW3b6R7N5Wq16FmYUhMpFC44snJb+NSg2d2rMZ3uIVir16zk8jb+an6AgPUvd7KhbSl5hJbU4EMUK3XNgOXC56C65c8Y6Ae7qKrdyVm54zlMphd/4DGeq6QyYaMSqbiZzZ9hQiWuoaSXIpZ/Zng0W6rgsI4QnSrMV0R7DOx104AfDBJ58qV9IPr1zN2GmDP0vg4PFLeP7Nn+BvfzwuhWW/9pkKvL/7EQx3fwHffrYZrS1+zFy9geHrn6qVgxBJYp2hucBbVMg7AhWd2RuZyn89azFPiCGApak54RjmK5tZak8eVSEvQjL6KLrb64mZaSvXFWf6JpleQMurP0J4IJaViK785e85pp80IZ1zhAdirkTkL/MIPiUoUhsowgPjeetLKSKnKOcScJt4rAnEhukJP078PsePdfpSQu6vTImorlEsKpUaYU1vJ4pKFAmjTr+plbQi9YBmIC4qsh24QFNNmQcF/jIP8a8qMvaPEYKTw1P49qPrceJnU+hLzOH6ws/tr54ankKV6fz75+hVXJtfwMXJaxj/5FP0M5scW3/zHjyy2ovhkX+1Lzi4yoO/fOpB6RqPvxdT+y644lRAoHrpGwC3rPMZkSJLdiqSA5NzN9ERHsSRpx/Os0tIvUfNX1acG3kKkR92YDqbFzP4lKRroFw4OdPkCTVXk0BVCe0Iv6/IeOZHWOehIQTrfDSTaeb1FJLzf/F5dB4apgf6LgvuCJ4QwwMx1Kzy0P2P836nYJ2PgBAq+8l4M6fr6AW0BirzVASPQiwFqyRHN1Vs9ZOYksH0J9tu8rFYswoI1vnsPV3LU3hzsjJze5qOSk0Tl5C7q9pS1KlnGLkAAHZ+rsbeY3UwmkDq0wW0PVSNr5UX4/Of5W3D+mf/AWUrCuBdcSdKigpRV3kPHvT48E1/Kap+w4PaGsbZGUvh8Mkr+C1fMb7wuSp1NOAnMUX0SPZj7Np639Kjbo2V6HxnWGH/8oO4NzKFjvAgFesL58UxLWwszaUUaENVCb/5T3CwJ+cXpegWi4Nnx9WrD8lt8jim1CNODoqUtm+ZUovoOnoBPR1NWe+ru72ebFlXTo0s3kUXuW/s99u9rVYiktZABdMuKgc6RXR2Hi2v/BCnvv5IfojIzZ/hKAySVRW7jQ0BNWyfSKWFIeVO5efeiBRtFCOd5sJG1apQrbZsNTh3U0FUfMS0wPJ8G5sjTWL4OIXeo6Yn/O/4Dt/pL0P4hUdyusfa6hI8Hwq4/v3IyajhC8pQZ9pQBgUZHbGZfCOtjZVO4X7VBl1z8IYHjEjNvu3rka2mdTK9QPsvJnD64rS0Y1m5WZMrh5vdWgiu86FLMoH4Xdwd4UGc2rNZmmjhgXHq1FlWO1n9q4rsa47OpKm3qFCpMq0clOhsmkq1m5lj9g5NoYdTAO4E2RqoIN6nNtGWV8+4mJ7GP/0XE5Kjc0egwlG23ATl/XyRiWtoefUMutvraa4lJ6IzaZpML3B9GZ1Jy66BpU5yrv+yjlf3Kg/UMA/ZfJ+8O97N/6srOVCFqcd7nlVjyM78zvDE5gJrsoY2VVOjuBnkGtLMxR68kkDL8UvYuX3tsu59bDyFL31v2LWsBotd2279WfXdT9QbA1eVcCaY0ZFYEm1v/hheTyENrC7BlnVOmYPxmbRRgyZh1tcxV5Hu9noiEJSiSD7ba9lvI1hXTvxlHrPECpWLvsHING159Qz2bV9P/b5iJNM3cfBsDNx+LTbsysyBnZ9z6jL1np9E5zvDaG2spDsCFQjWlXMmQjK9QKOJtCJK5ERIxKzfxhdPGmUrGiuUZSWMovIK9cjkqUTiSYmEQs01pOvYCOXrGxGl38Rqn0BViXFf68q5drHqCQ2Z9Y2is/PYva0WgaqNCt8JlFFO1p+jQurTRUViHx9gEPqdV+mKxaztzXM48tRnabbyLJF4ilrtLG5LSllpAKromWuGtKyC+fyvHNQgkbPHg3XlTqH77ifq0Ts0xT9vXmW3E4LQ0Q+NwXyLRDQ2nsKfvHmO8TdReeOk+avXU4Ddy0jF95d5SHf7RmqbZYQoNhMSTh0l0wvoNx8Zo6wlbDviirJYZbeej7Sz2TSRpTR8Z2BG4im0vXkOyuQ2bjuBY4J6PYVce1oToTcyZSfKWSScnL9pPH9OaS67O7f9q4oQnTG2hxzouwxQSq1IZSSWdIhatSKbA9bN+X7k6YfR+K0TkgByC6VHYklE4il0YURtZzETsMEt+qrM+jaKiWUkgliS/z6RlKY0VoN15ZTfBkPlgMArZxCo9tIdgQoEVpfA6yk0ttHML+D0aAKRiZRtBu17bL3aV0Vwa3WyiWJ7SravE0WkjlGHBaz03rd9vZEVKTnS5IkbOvohJhNzGc0tpQ/o1Dj++HtDThKkdHM8rfbsbFr2jujd22rJ0ETK2ZPDsTdVr6h2SFFROsFsB+VKmGX/Va7Y//gG8u7QJLVLeLgeRxVJhHoFphQ9ofuF9iTSxliLhJXtoZiQomLxlxcjOpsGm6FuPw1D9ew5ofSI9YBJN19VT8cDtCP8Pj9eiIszh7j552SzR7moqOprqQI+WVyDbmQrYt/2DegfPZP5WX/mxlSnsJ14QsqY9uXuFyftcROK+IvObNXinZWFmIxs8RrNt+4QJ2troEIRfqPKetMvDHyEhmf/Ad85finrpDr540m0ffME2v73oJODxD4eGkJZCHOjXr6eOdYTaiI9ofshZ6sSeTe25PUnQvF/4/cta32y885uYOry0L3cFdGRpzfBW1TATIAMu6iFXe2q6E5PRxPEekKcSeCWku+yTcI6txQ0EMOyXD0eVV0cfmwG15dndPiGmqtJT+gBM43ARWXSDKu4qiSKYRpw5zx9McFPOmn3eBY/UyINaYNylsUoWOcjoeZqdXtRRSkNEMUYJur+VY0dKz+I/an2mbmX/qDqHf6ReIryFg5bDcDp8ALFZEXUKmikonRB8g9fv4Gd736Ar/7jBfxBpRfrKu9hFmaKqeSnOB2/auQeqcwaQpWyMFDtRXd7fV6DAaHmGhKo8tLOQ8P8Qw/d1JgY5qb8xJejCNQ9oY5dgZdgSl75q993qhMKUSSVxJfMaFB4i1egu71eWS7V6ykkoJTy+UxQPOhScW5j4ZIm75Y6n1P7mkD9JFvRqWwlpFZ7ceSph3PoSyaNIJbKPNml0htMuQlrM7EiIqTcKpJDlEvpU6LqDeIuUUTjoYujCXVwItOxBNPP3YpQlOrNemsUygdfuBBXMn1T4d+yVJGzOBaoBuWpPZudga+Ss4qJd/3mz3EwOgNcSagVgMrcUq4oRip8XivjCXL+1J7N9uN9uX1akofNxQSyH/fiVThX4VpVMeMTHVxg5diEB8Zp19EL5ooEYTuH+4ofaq7Bvsc35La7XPnYazGiCPthAN3tG9VKleYQiaFi1M+I0na31+fc74GqEnL+L7YabXNsRHYkq8YsV5rWucVAtddl9VeQSM571RTmG8n+wEVzDmL/sQuUjVrzpK4w/8GPC7d66NbTZsUtTNb33HcmELkdLP9YJv+oFDgAV92hIEMjMNXdqDIqwmePumRtSklzRJ28RB0FdLsISFRFoeYaswSsEW6PJtKGU89+NBBv+FvO1UCVFyV3F0iOSb/Pg31WiQyqNtOzRVSyXW8knqK9EeNZ65FY0iy/4SiYQFUJAtVebFlXjtZARU7teP6bWxGJJxGdSeP0aEJ2SNtlJHzwr/JgR2MF3MrEMuoI/aPTGJowHKbOtTKO3TIPAlVGFLK1sfKWy3BYbdM/mqDvRibRf9EsM6oy/1T7v1aXKJM3/WUeJ59Ksa5m2rIRiadotgz/TAmjALD/sQ0k1FxjFLkfmlLWbeLvDVZgAYHVJdjRoL6+4LryjIJMldWfml9EcJ3P9Xsqs8/rWcEToaLcbaDKC0Iz5D7Yj0y2y6Tye9jUOQ0kgykCye/DyrlM5S00NJYK9hE18iygS3oKya8K7AqTLrZTXnOIfkHISEIWDvSN0a7jI072o0gyrkWOoFQ7YsTFW1SAnlBT3pzQGhoa/5+RkKWKnBR+5JDgpCgRIPz0FhVg17Za7N5aq9WPhoYmodzlYPi9cRwcGEd0dh7KUpQiOQmE5V9VhJ3NNdi9TZOPhoYmoWUUg7KcpGzquwqsAzJYV57fx+toaGj8+pKQhoaGxnJxh24CDQ0NTUIaGhqahDQ0NGQQQr5ICKE5vNYQQmC+mgghewkhs4rP/cD8m/X5QzkenxJCLjPnsF57FceYNd8vVXwehJD3hc9vE/5eKvy91OV98fWSy/lUbQjiBK6ofumXfrm8ALwEZxeZ2+t9hrf25vB5ah4XAGZz/Dz7HQDYBuByls/PAhBLXpYqPvcD4TNfdLm3L2Y53xcVPF7qcp1rLP7RSkhDIzO2Mb+/TCkl7AtGfsoD5t+fFIjiK3B2+hEAzwGwnhYxaP5cxfx9lXDudsX3rfP8wJrIAN5mjvMogI8YAjhk/lTdD/veGub/7O99zO+HheuAeS7r+g4rjv2kcDzpHJqENDTcTbFSQUkMLoGw+kxyYPEygPvM4/Rl+T5cPrMGwFvM/58zye4q852Xhc+zx3UrBr7X5TOqR+zk2iZrBFK+qjqGJiENjdxIxY0URNODnWQq8+SqqZxymdyqz7wlqJCXFZ/pc1Mdwj0dFkytUhcyzdQumUiIvda3hfOVahLS0MgO0YyQHM3CSt8nTLJDpv/jLUFpuKFJIBgVyW0TJvZS0SSYi6zp9qT591KGMAcV3y/NgZifZK71qkmWH2lzTEPj1iesGwYFc+s5lwlpObj35ni+wWUos1KF+lKZWVcFIntyiSroqst1lgr3aRHQVU1CGhq3bo49JzqlTce06Ix9GY7zVqVUXnIx03JRGGtcyCVXslIRzNvC8fdmIcKmHEhwL3OtrMn4keo4moQ0NBQghORqdrjhZTjRsa/koLByURi54knhuj/KoLRENVSahYTW5EBSe4XPu6UCrNEkpKGRmwq6SinNRgqXM5hvbwuqJZtT2o3wPsqieEQVAsE8dDO13s7B1LQIKpvf6qUltLEmIQ2NDMiFFNjJuQZGYt9bCnJ4S1AYh7OQnhvhHRYm/kvM90rN84h5SoMKc09UWqqUAVV0LptPSvQp3Qc+z4koVZXOitUv/VJmSmfLRqYwol9A9kxi9rXNRRFk+wxLjtmyrGcVfqe9Gcwii0DcsrMPZTmXRYKzLt8XFaP9GZ0xraGh9geVQp3l62auHDbNnudcPtdn/m2Vi6pqymIGiX+7D3z2teo8hxVqLdPx3xZU1mAWs09UQy8JSuvlHEzKNYCuJ6ShofFLhlZCGhoamoQ0NDQ0CWloaGhoEtLQ0NAkpKGhoaFJSENDQ5OQhoaGhiYhDQ0NTUIaGhoamoQ0NDQ0CWloaGhoEtLQ0NAkpKGhoZFX/PsAzCaGCLq0yPUAAAAASUVORK5CYII='
            ,width: 150,
            margin: [ 0, 10, 10, 20 ],
            alignment: 'right'
          },
          // pageMargins: [ 0, 0, 0, 0 ],
          content: [
            {
            text: 'AdBlue tankimise aruanne (' + moment(getJsDateFromExcel(billDate)).format('MMMM YYYY') + ')\n\n',
            style: 'header',
            margin: [ 0, 50, 0, 0 ]
            },
            {
            style: 'tableExample',
            table: {
              body: mergedArray,
              margin: [ 0, 20, 0, 0 ]
            },
            layout: {
              fillColor: function (rowIndex, node, columnIndex) {
                return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
              }
            }
          }],
          styles: {
            header: {
              fontSize: 14,
              bold: true,
              margin: [0, 0, 0, 10]
            },
            subheader: {
              fontSize: 16,
              bold: true,
              margin: [0, 10, 0, 5]
            },
            tableExample: {
              margin: [0, 5, 0, 15],
              fontSize: 8,
            },
            tableHeader: {
              bold: true,
              fontSize: 10,
              color: 'black'
            },
            footerStyle: {
              fillColor: 'black',
              color: 'white',
              margin: [0, 0, 0, 0],
            }
          },
        };
    
        pdfLinks.push({ client: clients[0].Client , pdf: await (this.generatePdf(docDefinition))});
      }
    }
  
    return pdfLinks;
  } catch(e) {
    return [];
  }
}