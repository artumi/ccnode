var bodyParser  = require('body-parser');
var async = require('asyncawait/async');
var await = require('asyncawait/await');

const multer = require('multer');

const { readExcel, splitPdfs, splitToDocs } = require('../utils');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
const upload = multer({storage: storage})

var getGeneratedPdfsLinks = async (function () {
  let fileContent = readExcel();
  let generatedPdfs = await (splitPdfs(fileContent, splitToDocs));
  return generatedPdfs;
});

module.exports = function(app) {
    app.use(function (req, res, next) {
  console.log('Time:', Date.now())
  next()
})

    app.post('/photos/upload/', upload.single('photos'), (req, res) => {
      res.json({
        success: true,
        message: 'File uploaded',
      });
    });

    app.post('/generatePdf/', (req, res) => {
      getGeneratedPdfsLinks().then(links => {
        res.send(links);
      })
    });

    app.post('/authExcel/', (req, res) => {
      const details = { 
        username: req.body.username,
        password: req.body.password
      };
      console.log(req.body);
      console.log(details);
      
      if (details.username == 'cross' && details.password == 'chemcross') {
        res.json({
          success: true,
          message: 'Welcome'
        });
      } else {
        res.status(401)        // HTTP status 404: NotFound
          .send('Access denied');
      }
    });

    app.get('/', (req, res) => {
      res.send("Hello");
    })
};
