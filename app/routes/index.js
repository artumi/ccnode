const ccRoutes = require('./cc_routes');
module.exports = function(app) {
  ccRoutes(app);
  // Other route groups could go here, in the future
};